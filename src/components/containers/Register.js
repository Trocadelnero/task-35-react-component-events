import React, { Fragment } from "react";
import RegisterForm from "../forms/RegisterForm";

class Register extends React.Component {
  state = {
    result: null,
  };

  componentDidMount() {}

  handleRegisterClicked = (result) => {
    console.log("Triggered from RegisterForm", result);
    if (result) {
      //Redir
    }
  };
  render() {
    return (
      <Fragment>
        <h1>Register to Survey Puppy</h1>
        <RegisterForm click={() => this.handleRegisterClicked()} />
      </Fragment>
    );
  }
}

export default Register;

// const Register = () => (
//   <div>
//     <h1>Register to Survey Puppy</h1>

//     <RegisterForm/>
//   </div>
// );
