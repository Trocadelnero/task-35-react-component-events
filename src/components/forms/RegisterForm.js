import React , { useState } from "react";
import { registerUser } from "../../api/user.api";


  const RegisterForm = (props) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState("");
    const [isRegistered, setIsRegistered] = useState(false);

    const onRegisterClicked = async (ev) => {

      setIsLoading(true);

      try {
        const result = await registerUser(username, password);
        console.log(result);
        if (result.status < 400) {
          setIsRegistered(true);
        }
        
      } catch (e) {
        setRegisterError(e.message || e);
      } finally {
        setIsLoading(false);
      }

    };

    const onUsernameChanged = (ev) => setUsername(ev.target.value.trim());
    const onPasswordChanged = (ev) => setPassword(ev.target.value.trim());

  return (
    <form>
      <React.Fragment>
        <label>username</label>
        <input 
        type="text" 
        placeholder="Enter a username"
        onChange={onUsernameChanged} 
        />
      </React.Fragment>

      <React.Fragment>
        <label>password</label>
        <input 
        type="password" 
        placeholder="Enter a password" 
        onChange={onPasswordChanged}
        />
      </React.Fragment>

      <React.Fragment>
        <button type="button" onClick={onRegisterClicked}>
        Register
        </button>
    </React.Fragment>

    { isLoading && <div>Registering User...</div> }
  { registerError && <div>{ registerError }</div> }
  { isRegistered && <div>You are now registered with Survey-Poodle!</div>}
    </form>
  );
};

export default RegisterForm;
